FROM openjdk:8-alpine

# starting application
RUN apk add /bin/sh

RUN mkdir -p /opt/app

ENV PROJECT_HOME /opt/app

COPY target/hello-javafx-maven-example-1.0-SNAPSHOT.jar $PROJECT_HOME/application.jar

WORKDIR $PROJECT_HOME

CMD ["java", "-jar application.jar" ]
